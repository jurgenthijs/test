stages:
  - preparation
  - building
  - testing
  - security

image: edbizarro/gitlab-ci-pipeline-php:7.4-alpine

variables:
  MYSQL_ROOT_PASSWORD: root
  MYSQL_USER: ohdear_ci
  MYSQL_PASSWORD: ohdear_secret
  MYSQL_DATABASE: ohdear_ci
  DB_HOST: mysql

composer:
  stage: preparation
  script:
    - php -v
    - composer install --prefer-dist --no-ansi --no-interaction --no-progress --no-scripts
    - cp .env.example.gitlab .env
    - php artisan key:generate
  artifacts:
    paths:
      - vendor/
      - .env
    expire_in: 1 days
    when: always
  cache:
    paths:
      - vendor/

yarn:
  stage: preparation
  script:
    - yarn --version
    - yarn install --pure-lockfile
  artifacts:
    paths:
      - node_modules
    expire_in: 1 days
    when: always
  cache:
    paths:
      - node_modules

build-assets:
  stage: building
  dependencies:
    - composer
    - yarn
  script:
    - yarn --version
    - yarn run production --progress false
  artifacts:
    paths:
      - public/css
      - public/js
      - public/fonts
      - public/mix-manifest.json
    expire_in: 1 days
    when: always

db-seeding:
  stage: building
  services:
    - name: mysql:8.0
      command: [ "--default-authentication-plugin=mysql_native_password" ]
  dependencies:
    - composer
    - yarn
  script:
    - mysql --version
    - php artisan migrate:fresh --seed
    - mysqldump --host="${DB_HOST}" --user="${MYSQL_USER}" --password="${MYSQL_PASSWORD}" "${MYSQL_DATABASE}" > db.sql
  artifacts:
    paths:
      - storage/logs # for debugging
      - db.sql
    expire_in: 1 days
    when: always

phpunit:
  stage: testing
  services:
    - name: mysql:8.0
      command: [ "--default-authentication-plugin=mysql_native_password" ]

  dependencies:
    - build-assets
    - composer
    - db-seeding
  script:
    - php -v
    - sudo cp /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini /usr/local/etc/php/conf.d/docker-php-ext-xdebug.bak
    - echo "" | sudo tee /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini
    - mysql --host="${DB_HOST}" --user="${MYSQL_USER}" --password="${MYSQL_PASSWORD}" "${MYSQL_DATABASE}" < db.sql
    - ./vendor/phpunit/phpunit/phpunit --version
    - php -d short_open_tag=off ./vendor/phpunit/phpunit/phpunit -v --coverage-text
    - sudo cp /usr/local/etc/php/conf.d/docker-php-ext-xdebug.bak /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini
  artifacts:
    paths:
      - ./storage/logs # for debugging
    expire_in: 1 days
    when: on_failure

codestyle:
  stage: testing
  image: lorisleiva/laravel-docker
  script:
    - phpcs --standard=PSR2 --extensions=php app
  dependencies: [ ]

phpcpd:
  stage: testing
  script:
    - test -f phpcpd.phar || curl -L https://phar.phpunit.de/phpcpd.phar -o phpcpd.phar
    - php phpcpd.phar app/ --min-lines=50
  dependencies: [ ]
  cache:
    paths:
      - phpcpd.phar

covarage:
  stage: testing
  image: edbizarro/gitlab-ci-pipeline-php:7.4
  script: 
    - php -d short_open_tag=off ./vendor/phpunit/phpunit/phpunit --coverage-text --colors=never

sensiolabs:
  stage: security
  script:
    - test -d security-checker || git clone https://github.com/sensiolabs/security-checker.git
    - cd security-checker
    - composer install
    - php security-checker security:check ../composer.lock
  dependencies: [ ]
  cache:
    paths:
      - security-checker/
